import cors from "cors";
import { randomUUID } from "crypto";
import express, { json } from "express";
import http from "http";
import { Server as SocketIo, Socket } from "socket.io";

interface ViewObject {
  id: string;
  type: string;
  text: string;
  alignmentX: boolean;
  alignmentY: boolean;
  offsetX: string;
  offsetY: string;
  width: string;
  height: string;
}

async function start() {
  const app = express();
  const server = new http.Server(app);
  const io = new SocketIo(server, {
    cors: {
      origin: ["http://media-manager.nkls.net", "http://localhost:4200", "*"],
    },
  });
  const sessions = new Map<
    string,
    {
      clients: Map<string, Socket>;
      objects: Map<string, ViewObject>;
      view: { width: number; height: number };
      password?: string;
    }
  >();
  app.use(cors());
  app.use(json());

  app.get<{ sessionId: string }, ViewObject[], never, never>(
    "/objects/:sessionId",
    (req, res) => {
      const objectArray: ViewObject[] = Array.from(
        sessions.get(req.params.sessionId)?.objects.values() || []
      );
      res.status(200).send(objectArray);
    }
  );

  app.post("/sessions", (req, res) => {
    console.log(req.body);

    const sessionId = `session-${randomUUID()}`;
    sessions.set(sessionId, {
      clients: new Map(),
      objects: new Map(),
      view: req.body.view,
      password: req.body.view.password,
    });
    res.status(201).send({ sessionId, password: req.body.view.password });
  });

  app.get<
    { sessionId: string },
    { width: number; height: number },
    never,
    never
  >("/sessions/:sessionId", (req, res) => {
    console.log(req.params.sessionId, sessions.values());
    const view = sessions.get(req.params.sessionId)?.view;
    if (!view) {
      res.sendStatus(404);
      return;
    }
    res.status(200).send(view);
  });

  // app.post<never, ViewObject, Omit<ViewObject, "id">, never>(
  //   "/objects",
  //   (req, res) => {
  //     const obj: ViewObject = { ...req.body, id: randomUUID() };
  //     clients.forEach((c) => c.emit("update-object", obj));
  //     objects.set(obj.id, obj);
  //     res.status(200).send(obj);
  //   }
  // );

  // app.put<{ id: string }, ViewObject, ViewObject, never>(
  //   "/objects/:id",
  //   (req, res) => {
  //     objects.set(req.params.id, req.body);
  //     clients.forEach((c) => c.emit("update-object", req.body));
  //     res.status(200).send(req.body);
  //   }
  // );

  // app.delete<{ id: string }, ViewObject, ViewObject, never>(
  //   "/objects/:id",
  //   (req, res) => {
  //     clients.forEach((c) =>
  //       c.emit("remove-object", objects.get(req.params.id))
  //     );
  //     res.status(200).send(objects.get(req.params.id));
  //     objects.delete(req.params.id);
  //   }
  // );

  io.on("connection", (client) => {
    console.log("connect", client.id);

    client.on(
      "join-session",
      (req: { sessionId: string; password: string }) => {
        const session = sessions.get(req.sessionId);
        if (!session) {
          console.log(`Session(${req.sessionId}) not found`);
          return;
        }

        if (session.password && session.password !== req.password) {
          console.log("Invalid auth", session.password, req.password);
          return;
        }

        client.join(req.sessionId);
        session.clients.set(client.id, client);
        console.log(`client(${client.id}) joined session(${req.sessionId})`);

        client.emit("init-objects", Array.from(session.objects.values()));

        client.on("remove-object", (obj: ViewObject) => {
          client.to(req.sessionId).emit("remove-object", obj);
          client.emit("remove-object", obj);
          session.objects.delete(obj.id);
        });

        client.on(
          "create-update-object",
          (object: Omit<ViewObject, "id"> & { id: string | undefined }) => {
            console.log("create-update-object", object, client.rooms);

            const sanitized = { ...object, id: object.id || randomUUID() };
            session.objects.set(sanitized.id, sanitized);
            client.to(req.sessionId).emit("create-update-object", sanitized);
            client.emit("create-update-object", sanitized);
          }
        );
      }
    );

    client.on("disconnect", () => {
      console.log("disconnect", client.id);
      sessions.forEach((session) => session.clients.delete(client.id));
    });
  });

  server.listen(3000);
}

start();
