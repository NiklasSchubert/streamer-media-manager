import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { ExtendedViewObject, ViewObject } from 'src/app/models/view-object';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class ObjectsService {
  public initObjects$ = this.socket.fromEvent<ViewObject[]>('init-objects');
  public updateObject$ = this.socket.fromEvent<ViewObject>(
    'create-update-object'
  );
  public removeObject$ = this.socket.fromEvent<ViewObject>('remove-object');

  constructor(private httpClient: HttpClient, private socket: Socket) {
    socket.connect();
  }

  public getSessions() {
    return this.httpClient.get<string[]>(`${environment.backendUrl}/sessions`);
  }

  public getSession(sessionId: string) {
    return this.httpClient.get<{ width: string; height: string }>(
      `${environment.backendUrl}/sessions/${sessionId}`
    );
  }

  public createSession(view: { width: number; height: number }) {
    return this.httpClient.post<{ sessionId: string; password: string }>(
      `${environment.backendUrl}/sessions`,
      { view }
    );
  }

  public joinSession(query: { sessionId: string; password?: string | null }) {
    this.socket.emit('join-session', query);
  }

  public getObjects(sessionId: string) {
    return this.httpClient.get<ViewObject[]>(
      `${environment.backendUrl}/objects/${sessionId}`
    );
  }

  public createUpdateObject(object: ViewObject | ExtendedViewObject) {
    this.socket.emit('create-update-object', object);
  }

  public createObject(object: ViewObject | ExtendedViewObject) {
    return this.httpClient.post<ViewObject>(
      `${environment.backendUrl}/objects`,
      object
    );
  }

  public updateObject(object: ViewObject | ExtendedViewObject) {
    return this.httpClient.put<ViewObject>(
      `${environment.backendUrl}/objects/${object.id}`,
      object
    );
  }

  public removeObject(object: ViewObject | ExtendedViewObject) {
    this.socket.emit('remove-object', object);
  }
}
