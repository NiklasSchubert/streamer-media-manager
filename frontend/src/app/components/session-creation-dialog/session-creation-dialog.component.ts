import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-session-creation-dialog',
  templateUrl: './session-creation-dialog.component.html',
  styleUrls: ['./session-creation-dialog.component.scss'],
})
export class SessionCreationDialogComponent implements OnInit {
  public form = new FormGroup({
    width: new FormControl('1920px', [Validators.required]),
    height: new FormControl('1080px', [Validators.required]),
    password: new FormControl(''),
  });
  constructor() {}

  ngOnInit(): void {}
}
