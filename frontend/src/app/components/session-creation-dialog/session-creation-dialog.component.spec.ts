import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionCreationDialogComponent } from './session-creation-dialog.component';

describe('SessionCreationDialogComponent', () => {
  let component: SessionCreationDialogComponent;
  let fixture: ComponentFixture<SessionCreationDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SessionCreationDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionCreationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
