import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ViewObject } from 'src/app/models/view-object';

@Component({
  selector: 'app-object-creation-dialog',
  templateUrl: './object-creation-dialog.component.html',
  styleUrls: ['./object-creation-dialog.component.scss'],
})
export class ObjectCreationDialogComponent implements OnInit {
  public options: { value: string; viewValue: string; caption?: string }[] = [
    {
      value: 'image',
      viewValue: 'Image',
      caption: 'direct links to images like png,jpg,etc',
    },
    {
      value: 'video',
      viewValue: 'Video',
      caption: 'direct links to videos like mp4, webm, etc',
    },
    {
      value: 'audio',
      viewValue: 'Audio',
      caption: 'direct links to audio like ',
    },
    { value: 'text', viewValue: 'Text' },
    { value: 'iframe', viewValue: 'Iframe' },
  ];

  public form = new FormGroup({
    id: new FormControl<string | undefined>(undefined, []),
    type: new FormControl('image', [Validators.required]),
    text: new FormControl('', [Validators.required]),
    font: new FormControl('', []),
    fontSize: new FormControl('20px', [Validators.required]),
    fontColor: new FormControl('0x000', [Validators.required]),
    volume: new FormControl(50, [Validators.required]),
    transparency: new FormControl(100, [Validators.required]),
    offsetX: new FormControl('0px', [Validators.required]),
    offsetY: new FormControl('0px', [Validators.required]),
    width: new FormControl('', []),
    height: new FormControl('', []),
    visible: new FormControl(false, []),
  });

  constructor(@Inject(MAT_DIALOG_DATA) public data?: ViewObject) {
    if (data) {
      this.form.patchValue(data);
    }
  }

  ngOnInit(): void {}
}
