import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjectCreationDialogComponent } from './object-creation-dialog.component';

describe('ObjectCreationDialogComponent', () => {
  let component: ObjectCreationDialogComponent;
  let fixture: ComponentFixture<ObjectCreationDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ObjectCreationDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjectCreationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
