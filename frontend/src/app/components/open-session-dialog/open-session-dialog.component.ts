import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-open-session-dialog',
  templateUrl: './open-session-dialog.component.html',
  styleUrls: ['./open-session-dialog.component.scss'],
})
export class OpenSessionDialogComponent implements OnInit {
  public form = new FormGroup({
    id: new FormControl('', Validators.required),
    password: new FormControl(''),
  });
  constructor() {}

  ngOnInit(): void {}
}
