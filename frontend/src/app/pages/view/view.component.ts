import { CdkDragEnd, CdkDragMove, CdkDragStart } from '@angular/cdk/drag-drop';
import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Socket } from 'ngx-socket-io';
import { combineLatest } from 'rxjs';
import { ExtendedViewObject, ViewObject } from 'src/app/models/view-object';
import { ObjectsService } from 'src/app/services/objects/objects.service';
import { load } from 'webfontloader';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
})
export class ViewComponent implements OnChanges, OnInit {
  @Input()
  public externalData: ViewObject[] = [];

  @Input()
  public external = false;

  @Input()
  public showInvisible = false;

  public data: ExtendedViewObject[] = [];
  public userAgent = navigator.userAgent;

  constructor(
    public sanitizer: DomSanitizer,
    private objectService: ObjectsService,
    private socket: Socket,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    if (!this.external) {
      this.objectService.initObjects$.subscribe((objects) => {
        this.data = objects.map((v) => this.createStyle(v));
        this.loadFontsForList(this.data);
      });

      this.objectService.updateObject$.subscribe((object) => {
        this.data = this.addToList(this.data, object).map((v) =>
          this.createStyle(v)
        );
        this.loadFontsForList(this.data);
      });

      this.objectService.removeObject$.subscribe((object) => {
        this.data = this.data.filter(
          (viewObject) => object.id !== viewObject.id
        );
      });

      combineLatest([
        this.activatedRoute.queryParams,
        this.activatedRoute.params,
      ]).subscribe(([query, params]) => {
        this.objectService.joinSession({
          sessionId: params['id'],
          password: query['password'],
        });
      });
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.external) {
      this.data = this.externalData
        .filter((entry) => entry.visible || this.showInvisible)
        .map((entry) => this.createStyle(entry));
      this.loadFontsForList(this.data);
    }
  }

  public createStyle(entry: ViewObject): ExtendedViewObject {
    const entryWithStyle: ExtendedViewObject = {
      ...entry,
      style: {
        // [entry.alignmentX ? 'right' : 'left']: entry.offsetX,
        // [entry.alignmentY ? 'bottom' : 'top']: entry.offsetY,
        width: entry.width,
        height: entry.height,
        color: entry.fontColor,
        'font-family': entry.font,
        fontSize: entry.fontSize,
        opacity: entry.transparency / 100,
      },
      dragPosition: {
        x: Number(entry.offsetX.replace('px', '')),
        y: Number(entry.offsetY.replace('px', '')),
      },
    };

    if (entryWithStyle.type !== 'text') {
      console.log(this.sanitizer);

      (entryWithStyle as ExtendedViewObject).sanitizedURL =
        this.sanitizer.bypassSecurityTrustResourceUrl(entryWithStyle.text);
    }

    return entryWithStyle;
  }

  public dragStart(event: CdkDragStart, media: ExtendedViewObject) {
    media.dragging = true;
  }

  public dragMove(event: CdkDragMove, media: ExtendedViewObject) {
    const { x, y } = event.source.getFreeDragPosition();
    media.offsetX = `${x}px`;
    media.offsetY = `${y}px`;

    const s = { ...media };
    delete s.dragging;
    this.socket.emit('update-object', s);
  }

  public dragEnd(event: CdkDragEnd | CdkDragMove, media: ExtendedViewObject) {
    const { x, y } = event.source.getFreeDragPosition();
    media.offsetX = `${x}px`;
    media.offsetY = `${y}px`;
    this.socket.emit('create-update-object', media);
  }

  public loadFontsForList(data: ViewObject[]) {
    data
      .filter((entry) => !!entry.font)
      .forEach(async (entry) => {
        load({
          google: {
            families: [entry.font],
          },
        });
      });
  }

  public addToList(list: ViewObject[], object: ViewObject) {
    const found = list.find((entry) => entry.id === object.id);
    if (found) {
      Object.assign(found, object);
    } else {
      list = list.concat(object);
    }

    return list;
  }
}
