import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { filter, switchMap } from 'rxjs';
import { OpenSessionDialogComponent } from 'src/app/components/open-session-dialog/open-session-dialog.component';
import { SessionCreationDialogComponent } from 'src/app/components/session-creation-dialog/session-creation-dialog.component';
import { ObjectsService } from 'src/app/services/objects/objects.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  constructor(
    private objectService: ObjectsService,
    private router: Router,
    private dialog: MatDialog
  ) {}

  createSession() {
    this.dialog
      .open(SessionCreationDialogComponent, { width: '500px' })
      .afterClosed()
      .pipe(
        filter((view) => !!view),
        switchMap((view) => this.objectService.createSession(view))
      )
      .subscribe(({ sessionId: id, password }) => {
        localStorage.setItem('session-password', password);
        this.router.navigate(['editor', id]);
      });
  }

  openSession() {
    this.dialog
      .open(OpenSessionDialogComponent, { width: '500px' })
      .afterClosed()
      .pipe(filter(({ id }) => !!id))
      .subscribe(({ id, password }) => {
        console.log(id, password);
        localStorage.setItem('session-password', password);
        this.router.navigate(['editor', id]);
      });
  }
}
