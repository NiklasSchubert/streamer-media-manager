import { Clipboard } from '@angular/cdk/clipboard';
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { switchMap, tap } from 'rxjs';
import { ObjectCreationDialogComponent } from 'src/app/components/object-creation-dialog/object-creation-dialog.component';
import { ViewObject } from 'src/app/models/view-object';
import { ObjectsService } from 'src/app/services/objects/objects.service';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss'],
})
export class EditorComponent {
  public dataSource = new MatTableDataSource<ViewObject>();
  public width = '1920px';
  public height = '1080px';

  public displayedColumns: string[] = [
    'type',
    'textLink',
    'offsetX',
    'offsetY',
    'width',
    'height',
    'visible',
    'actions',
  ];

  constructor(
    private matDialog: MatDialog,
    private objectService: ObjectsService,
    private clipboard: Clipboard,
    private activatedRoute: ActivatedRoute
  ) {
    this.objectService.initObjects$.subscribe((res) => {
      this.dataSource.data = res;
    });

    this.objectService.updateObject$.subscribe((object) => {
      this.dataSource.data = [...this.addToList(this.dataSource.data, object)];
    });

    this.objectService.removeObject$.subscribe((object) => {
      this.dataSource.data = this.dataSource.data.filter(
        ({ id }) => id !== object.id
      );
    });

    this.activatedRoute.params
      .pipe(
        tap((params) =>
          this.objectService.joinSession({
            sessionId: params['id'],
            password: localStorage.getItem('session-password'),
          })
        ),
        switchMap((params) => this.objectService.getSession(params['id']))
      )
      .subscribe((session) => {
        this.width = session.width;
        this.height = session.height;
      });
  }

  copyURL() {
    this.clipboard.copy(
      `${location.host}/view/${
        this.activatedRoute.snapshot.params['id']
      }?password=${localStorage.getItem('session-password')}`
    );
  }

  copySessionId() {
    this.clipboard.copy(this.activatedRoute.snapshot.params['id']);
  }

  public openDialog(object?: ViewObject) {
    this.matDialog
      .open(ObjectCreationDialogComponent, {
        width: '500px',
        data: object,
      })
      .afterClosed()
      .subscribe({
        next: (res) => {
          if (res) {
            this.objectService.createUpdateObject(res);
          }
        },
      });
  }

  public deleteObject(object: ViewObject) {
    this.objectService.removeObject(object);
  }

  public changeVisibility(obj: ViewObject, visible: boolean) {
    this.objectService.createUpdateObject({ ...obj, visible });
  }

  public addToList(list: ViewObject[], object: ViewObject) {
    const found = list.find((entry) => entry.id === object.id);
    if (found) {
      Object.assign(found, object);
    } else {
      list = list.concat(object);
    }

    return list;
  }
}
