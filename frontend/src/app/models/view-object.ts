import { SafeUrl } from '@angular/platform-browser';

export interface ViewObject {
  id: string;
  type: 'text' | 'link' | 'iframe' | 'audio' | 'video' | 'image';
  text: string;
  /*
   * text specific properties
   */
  font: string;
  fontSize: string;
  fontColor: string;
  /**
   * link specific properties
   */
  volume: number;
  transparency: number;
  offsetX: string;
  offsetY: string;
  width: string;
  height: string;
  visible: boolean;
}

export type ExtendedViewObject = ViewObject & {
  style: any;
  sanitizedURL?: SafeUrl;
  dragPosition: { x: number; y: number };
  dragging?: boolean;
};
